﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wander : MonoBehaviour {

    private float heading;
    public float speed;
    public float turnSpeed;
    private bool isAtWall = false;
    public float maxSpeed;

	// Use this for initialization
	void Start () {
        //randomizes the rotation and speed
        heading = Random.Range(-80f, 80f);
        Vector3 targetRot = new Vector3(0f, 0f, heading);
        speed = Random.Range(1f, maxSpeed);
        this.transform.eulerAngles = targetRot;
	}
	
	// Update is called once per frame
	void Update () {
        
        this.transform.Translate(Vector3.down.normalized * speed * Time.deltaTime);
        float probability = 3 * Time.deltaTime;

        Quaternion targetRot = new Quaternion();
        //moves away from wall
        if (isAtWall || transform.position.x <= -2.5f || transform.position.x >= 2.5f)
        {
            targetRot.eulerAngles = new Vector3(0f, 0f, transform.rotation.z * -1);
        }

        //random direction changes
        if (Random.Range(0f, 1f) < probability)
        {
            float heading = Random.Range(-90f, 90f);
            targetRot.eulerAngles = new Vector3(0f, 0f, heading);
        }

        transform.rotation = Quaternion.Slerp(transform.rotation, targetRot, Time.deltaTime * turnSpeed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Boundary")
            isAtWall = true;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Boundary")
            isAtWall = false;
    }

}
