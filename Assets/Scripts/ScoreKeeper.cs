﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//manages the score
public class ScoreKeeper : MonoBehaviour {

    public int score = 0;
    public Text scoreText;

    private GameObject spawner;

    private bool didUpdate = false;
 

	// Use this for initialization
	void Start () {
        Score(0);
        spawner = GameObject.Find("Spawner");
	}
	
	// Increases difficulty with every 1000 points
	void Update () {
        if ((score % 1000 == 0) && (score > 0) && !didUpdate)
        {
            didUpdate = true;
            spawner.GetComponent<BugSpawner>().increaseDifficulty();
        }
        else if (score % 1000 != 0)
        {
            didUpdate = false;
        }
    }

    //Updates score
    public void Score(int points)
    {
        score += points;
        scoreText.text = "" + score;
    }

    public void Reset()
    {
        score = 0;
        scoreText.text = "0";
    }

    
}
