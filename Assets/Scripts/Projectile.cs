﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {
    private float damage = 100f;

    public void Update()
    {
        Destroy(gameObject, 5f);
    }

    public void Hit()
    {
        Destroy(gameObject);
    }

    public float GetDamage() {
        return damage;
    }

}
