﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public float health = 300f;
    public float speed;
    public float poisonSpeed;
    public float firingRate;

    private GameObject scoreKeeper;
    public GameObject poison;

    private Animator anim;

    //clamping
    public float xmin;
    public float xmax;


	// Use this for initialization
	void Start () {
        //get scorekeeper
        scoreKeeper = GameObject.Find("ScoreKeeper");
        //gets animator
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        //movement handler
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
            anim.SetBool("isMoving", true);
        }
        else if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
            anim.SetBool("isMoving", true);
        }
        else if ((Input.GetKeyUp(KeyCode.RightArrow)) || (Input.GetKeyUp(KeyCode.LeftArrow)))
        {
            anim.SetBool("isMoving", false);
        }

        //restricting player position
        float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
        transform.position = new Vector3(newX, transform.position.y, transform.position.z);

        //if player presses space, start spitting at an interval
        if (Input.GetKeyDown(KeyCode.Space))
        {
            InvokeRepeating("Fire", 0.00001f, firingRate);
            anim.SetBool("isAttacking", true);
        }

        //if player lifts space, stop spitting
        if (Input.GetKeyUp(KeyCode.Space))
        {
            CancelInvoke("Fire");
            anim.SetBool("isAttacking", false);
        }
    }

    //fire spit
    void Fire()
    {
        GameObject newPoison = Instantiate(poison, transform.position, Quaternion.identity);
        newPoison.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, poisonSpeed, 0f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    { 
        Projectile proj = collision.gameObject.GetComponent<Projectile>();
        //if the collision is with a poison projectile then decrease health
        if (proj)
        {
            proj.Hit();
            health -= proj.GetDamage();
            Debug.Log(health);

            if (health <= 0)
            {
                Destroy(gameObject);
                scoreKeeper.GetComponent<ScoreKeeper>().Reset();
            }
                GetComponent<HealthManager>().UpdateHealth();
        }
    }
}