﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    public GameObject enemy;
    public float width = 10f;
    public float height = 5f;
    public float speed;
    private bool atBoundary;
    private float xmin;
    private float xmax;
    public float padding;
    public float spawnDelay = 0.5f;

	// Use this for initialization
	void Start () {
        GetBoundaries();
        SpawnUntilFull();
	}

    public void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(transform.position, new Vector3(width, height));
    }

    // Update is called once per frame
    void Update () {
        CheckBoundaries();

        //reverses direction when at the boundary
        if (atBoundary) { 
            speed *= -1;
        }

        transform.position += Vector3.right * speed * Time.deltaTime;

        if (AllEnemiesDefeated())
            SpawnUntilFull();
	}

    private void SpawnEnemies()
    {
        //Instantiate enemies at each position
        foreach (Transform child in transform)
        {
            GameObject newEnemy = Instantiate(enemy, child.transform.position, Quaternion.identity);
            newEnemy.transform.SetParent(child);
        }
    }

    private void SpawnUntilFull()
    {
        Transform freePosition = NextFreePosition();
        if (freePosition)
        {
            GameObject newEnemy = Instantiate(enemy, freePosition.position, Quaternion.identity);
            newEnemy.transform.SetParent(freePosition);
        }

        if(NextFreePosition())
            Invoke("SpawnUntilFull", spawnDelay);
    }

    //checks if the formation is at the boundary
    private void CheckBoundaries()
    {
        if (transform.position.x <= xmin || transform.position.x >= xmax)
            atBoundary = true;
        else
            atBoundary = false;
    }

    private void GetBoundaries()
    {
        //z distance from camera to the player
        float distanceZ = transform.position.z - Camera.main.transform.position.z;

        //gets leftmost and rightmost coordinates of the main camera.
        Vector3 leftmost = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, distanceZ));
        Vector3 rightmost = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, distanceZ));
        xmin = leftmost.x + padding;
        xmax = rightmost.x - padding;
    }

    bool AllEnemiesDefeated()
    {
        //iterate through each Position object and figure out if there is an enemy or not
        foreach(Transform childPositionGameObject in transform)
        {
            if (childPositionGameObject.childCount > 0)
                return false;
        }

        return true;
    }

    Transform NextFreePosition()
    {
        //iterate through each Position object and figure out if there is an enemy or not
        foreach (Transform childPositionGameObject in transform)
        {
            if (childPositionGameObject.childCount == 0)
                return childPositionGameObject;
        }

        return null;
    }
}
