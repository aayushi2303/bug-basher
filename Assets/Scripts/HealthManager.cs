﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

    //gui
    public Image[] hearts;
    //index of health value to remove
    public int currentHealth = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //removes health icon as needed
    public void UpdateHealth()
    {
        hearts[currentHealth].enabled = false;
        currentHealth++;
    }
}
