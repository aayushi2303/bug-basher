﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//script to spawnw enemy bugs
public class BugSpawner : MonoBehaviour {

    private float y;
    private float fixedZ = 0f;
    private float x;

    private int numberOfBugs;

    //variables to be modified
    public int maxNumberOfBugs;
    public float maxSpeed;

    public GameObject bugPrefab;

    //Checks if bugs can be spawned and spawns them
    void Update() {
        numberOfBugs = GameObject.FindGameObjectsWithTag("Enemy").Length;

        if(numberOfBugs == 0)
            SpawnBug(); 
	}

    void SpawnBug()
    {
        //spawns the max number of bugs within a random x and y range
        for (int i = 0; i < maxNumberOfBugs; i++)
        {
            x = Random.Range(-2f, 2f);
            y = Random.Range(5.4f, 9f);
            Vector3 spawnPoint = new Vector3(x, y, fixedZ);
            GameObject newBug = (GameObject)Instantiate(bugPrefab, spawnPoint, Quaternion.identity);
            newBug.GetComponent<Wander>().maxSpeed = maxSpeed;
        }

    }

    //increases difficulty parameters.. TODO: Promote to variable?
    public void increaseDifficulty()
    {
        maxSpeed += 0.5f;
        maxNumberOfBugs += 1;
    }
}
