﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float health = 100f;
    public GameObject poison;
    private GameObject scoreKeeper;

    public float poisonSpeed;
    public float spitsPerSecond = 0.2f;

    private float maxHeightToSpit = 5f;

    //clamping
    public float xmin;
    public float xmax;

    private void Start()
    {
        //reference to scorekeeper object
        scoreKeeper = GameObject.Find("ScoreKeeper");

        //projectile speed is the speed of the enemy + a fixed value
        poisonSpeed = GetComponent<Wander>().speed + poisonSpeed;
    }


    //if hit by poison, decrements health
    private void OnTriggerEnter2D(Collider2D collision){
        //check if other object is a Projectile
        Projectile proj = collision.gameObject.GetComponent<Projectile>();
        if (proj)
        {
            //deducts damage from health
            proj.Hit();
            health -= proj.GetDamage();

            //checks for 0 health and updates score
            if (health <= 0)
            {
                Destroy(gameObject);
                scoreKeeper.GetComponent<ScoreKeeper>().Score(100);
            }
        }
    }

    private void Update(){
        float probability = spitsPerSecond * Time.deltaTime;
        //fires depending on a probability and if the enemy is within range
        if ((Random.value < probability) && this.transform.position.y < maxHeightToSpit) { 
            Fire();
        }

        //clamps position
        //restricting player position
        float newX = Mathf.Clamp(transform.position.x, xmin, xmax);
        transform.position = new Vector3(newX, transform.position.y, transform.position.z);
    }

    //spit poison
    void Fire(){
        GameObject newPoison = Instantiate(poison, transform.position, Quaternion.identity);
        newPoison.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, -poisonSpeed, 0f);
    }

}
